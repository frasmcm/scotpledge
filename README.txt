## README

This dataset was assembled in 2012/2013 for my undergraduate dissertation project at the University of Aberdeen. It contains every pledge made in education and justice/law and order issue areas by the first three Scottish Governments after the Parliament opened in 1999:

- Labour and Liberal Democrat coalition, 1999-2003
- Labour and Liberal Democrat coalition, 2003-2007
- Scottish National Party minority, 2007-2011

Pledges were coded by "specificity" on a four-point scale (adapted from Bara 2005), with the "vague" category equivalent to a "non-testable" promise in the typical Comparative Party Pledge parlance. Guidelines for the specificity coding can be found in the same directory as this readme and codebook. I did a small inter-coder reliability check on this scheme at the time and it worked well.

There are 600 pledges in the dataset, 370 of which meet the testability criterion. These were checked for fulfilment using online government and media sources and coded as fulfilled/partly fulfilled/not fulfilled per convention. Duplicate pledges (i.e. repeat pledges in the same manifesto) were removed at the time I originally made the dataset and are not included.

The observations are individual pledges, which correspond to rows. The dataset is very clean and only contains a small number of variables. The text of each pledge is included so it should be possible to conduct text analysis and add more information in retrospect e.g. type of action promised.

I documented fulfilment sources, duplicate pledges and a lot of other things at the time. Please get in touch if you would like to see any of this or indeed for any other reason. 

I eventually published a paper adapted from the dissertation using this data in British Politics: https://doi.org/10.1057/s41293-019-00120-9

Email: fraser.mcmillan@glasgow.ac.uk

